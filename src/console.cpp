#include <console.hpp>
#include <iostream>
#include <sstream>

namespace inkwell {
  LogSource::LogSource(std::string name) {
    this->name = name;
    this->spaces = "";
    int spacec = 10 - name.length();
    for(int i = 0; i < spacec; i++) {
      spaces += " ";
    }
  }
  std::string Console::setColor(int n) {
    std::stringstream ss;
    ss << "\033[3" << n << "m";
    return ss.str();
  }
  std::string Console::resetStyle() {
    return "\033[0m";
  }
  std::string Console::setBold() {
    return "\033[1m";
  }
  std::string Console::setUnBold() {
    return "\033[21m\033[22m";
  }
  void Console::log(const LogSource *ls, std::string msg) {
    if(msg.empty()) return;
    std::cout << setBold() << "[" << ls->getName() << "] " << setUnBold() << ls->getSpaces() << msg << std::endl;
  }
  void Console::err(const LogSource *ls, std::string msg) {
    if(msg.empty()) return;
    std::cerr << setColor(1) << setBold() << "[" << ls->getName() << "] " << setUnBold() << ls->getSpaces() << msg << resetStyle() << std::endl;
  }
}
