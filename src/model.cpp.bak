#include<model.hpp>
#include<shader.hpp>
#include<constants.hpp>
#include<GL/glew.h>

namespace inkwell {
  Model::Model() {
    struct shader_attributes attribs[] = {
      //Front
      {{-1.0, -1.0,  1.0}, {0.0, 0.0}},
      {{ 1.0, -1.0,  1.0}, {1.0, 0.0}},
      {{ 1.0,  1.0,  1.0}, {1.0, 1.0}},
      {{-1.0,  1.0,  1.0}, {0.0, 1.0}},
      //Top
      {{-1.0,  1.0,  1.0}, {0.0, 0.0}},
      {{ 1.0,  1.0,  1.0}, {1.0, 0.0}},
      {{ 1.0,  1.0, -1.0}, {1.0, 1.0}},
      {{-1.0,  1.0, -1.0}, {0.0, 1.0}},
      //Back
      {{-1.0, -1.0, -1.0}, {0.0, 0.0}},
      {{ 1.0, -1.0, -1.0}, {1.0, 0.0}},
      {{ 1.0,  1.0, -1.0}, {1.0, 1.0}},
      {{-1.0,  1.0, -1.0}, {0.0, 1.0}},
      //Bottom
      {{-1.0, -1.0,  1.0}, {0.0, 0.0}},
      {{ 1.0, -1.0,  1.0}, {1.0, 0.0}},
      {{ 1.0, -1.0, -1.0}, {1.0, 1.0}},
      {{-1.0, -1.0, -1.0}, {0.0, 1.0}},
      //Left
      {{-1.0, -1.0, -1.0}, {0.0, 0.0}},
      {{-1.0, -1.0,  1.0}, {1.0, 0.0}},
      {{-1.0,  1.0,  1.0}, {1.0, 1.0}},
      {{-1.0,  1.0, -1.0}, {0.0, 1.0}},
      //Right
      {{ 1.0, -1.0,  1.0}, {0.0, 0.0}},
      {{ 1.0, -1.0, -1.0}, {1.0, 0.0}},
      {{ 1.0,  1.0, -1.0}, {1.0, 1.0}},
      {{ 1.0,  1.0,  1.0}, {0.0, 1.0}},
    };
    GLushort cube_elements[] = {
      // front
      0, 1, 2,
      2, 3, 0,
      // top
      4, 5, 6,
      6, 7, 4,
      // back
      8, 9, 10,
      10, 11, 8,
      // bottom
      12, 13, 14,
      14, 15, 12,
      // left
      16, 17, 18,
      18, 19, 16,
      // right
      20, 21, 22,
      22, 23, 20,
    };
    vertices = 36;
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(attribs), attribs, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cube_elements), cube_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    texture = new sf::Texture();
    if(!texture->loadFromFile("data/image/texture.jpg")) {
      console->err(&logsource_render, std::string("Failed to load texture"));
    }
  }
  GLuint Model::getVBO() const {
    return vbo;
  }
  GLuint Model::getIBO() const {
    return ibo;
  }
  int Model::getVertices() const {
    return vertices;
  }
  Model::~Model() {
    glDeleteBuffers(1, &vbo);
  }
}
