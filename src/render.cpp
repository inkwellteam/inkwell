#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <SFML/Graphics.hpp>
#include <GL/glew.h>
#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <render.hpp>
#include <constants.hpp>
#include <shader.hpp>
#include <entity.hpp>
#include <model.hpp>

namespace inkwell {
  void Render::computeViewProjection() {
    //glm::mat4 view = glm::lookAt(glm::vec3(0.0, 2.0, 0.0), glm::vec3(0.0, 0.0, -4.0), glm::vec3(0.0, 1.0, 0.0));
    glm::mat4 view(1.0f);
    glm::mat4 projection = glm::perspective(0.785398163f, float(vwidth)/float(vheight), 0.1f, 10.0f);
    vp = projection * view;
  }
  glm::mat4 Render::computeMatrix(Entity *e) const {
    return glm::translate(glm::mat4(1.0f), glm::vec3(e->x, e->y, e->z))
      * glm::rotate(glm::mat4(1.0f), e->rx, glm::vec3(1.0, 0.0, 0.0))
      * glm::rotate(glm::mat4(1.0f), e->ry, glm::vec3(0.0, 1.0, 0.0))
      * glm::rotate(glm::mat4(1.0f), e->rz, glm::vec3(0.0, 0.0, 1.0));
  }
  bool Render::init() {
    console->log(&logsource_render, std::string("Initializing Render Engine"));
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    GLenum err = glewInit();
    if(err != GLEW_OK) {
      console->err(&logsource_render, std::string("Failed to load OpenGL"));
      exit(1);
    } if(!GLEW_VERSION_2_1) {
      console->err(&logsource_render, std::string("We require at least OpenGL 2.1!"));
      exit(1);
    }
    triangle = new Entity(new Model());
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    if(!loadShaders()) { return false; }
    return true;
  }
  bool Render::loadShaders() {
    this->shaderLiteral = new Shader("literal.f.glsl", "literal.v.glsl");
    //delete this->shaderLiteral;
    //this->shaderLiteral = new Shader("literal.f.glsl", "literal.v.glsl");
  }
  void Render::tick(double time) {
    triangle->z = -4;
    //triangle->rx = time * 45.0;
    triangle->rx = 0;
    triangle->ry = time * 0.2;
    triangle->rz = 0;
  }
  void Render::render() {
    glClearColor(0.0f, 0.5f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    computeViewProjection();
    renderEntity(triangle);
    GLenum err;
    while((err = glGetError()) != GL_NO_ERROR) {
      std::stringstream ss;
      ss << "OpenGL Error: ";
      switch(err) {
      case GL_INVALID_ENUM: ss << "GL_INVALID_ENUM"; break;
      case GL_INVALID_VALUE: ss << "GL_INVALID_VALUE"; break;
      case GL_INVALID_OPERATION: ss << "GL_INVALID_OPERATION"; break;
      case GL_STACK_OVERFLOW: ss << "GL_STACK_OVERFLOW"; break;
      case GL_STACK_UNDERFLOW: ss << "GL_STACK_UNDERFLOW"; break;
      case GL_OUT_OF_MEMORY: ss << "GL_OUT_OF_MEMORY"; break;
      case GL_INVALID_FRAMEBUFFER_OPERATION: ss << "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
      default: ss << "Unknown Error: 0x" << std::hex << err;
      }
      console->err(&logsource_render, ss.str());
    }
  }
  void Render::renderEntity(Entity *e) {
    shaderLiteral->bind();
    glEnableVertexAttribArray(shaderLiteral->attribute_coord3d);
    glEnableVertexAttribArray(shaderLiteral->attribute_normal);
    glEnableVertexAttribArray(shaderLiteral->attribute_absnormal);
    glEnableVertexAttribArray(shaderLiteral->attribute_texcoord);
    glBindBuffer(GL_ARRAY_BUFFER, e->getModel()->getVBO());
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, e->getModel()->getIBO());
    glVertexAttribPointer(
                          shaderLiteral->attribute_coord3d,
                          3,
                          GL_FLOAT,
                          GL_FALSE,
                          sizeof(MVertex),
                          (GLvoid*) offsetof(MVertex, point)
                          );
    glVertexAttribPointer(
                          shaderLiteral->attribute_normal,
                          3,
                          GL_FLOAT,
                          GL_FALSE,
                          sizeof(MVertex),
                          (GLvoid*) offsetof(MVertex, norm)
                          );
    glVertexAttribPointer(
                          shaderLiteral->attribute_absnormal,
                          3,
                          GL_FLOAT,
                          GL_FALSE,
                          sizeof(MVertex),
                          (GLvoid*) offsetof(MVertex, absnorm)
                          );
    glVertexAttribPointer(
                          shaderLiteral->attribute_texcoord,
                          2,
                          GL_FLOAT,
                          GL_FALSE,
                          sizeof(MVertex),
                          (GLvoid*) offsetof(MVertex, texcoord)
                          );
    sf::Texture::bind(e->getModel()->texture);
    glm::mat4 modelmatrix = computeMatrix(e);
    glm::mat4 mvp = vp * modelmatrix;
    glUniformMatrix3fv(shaderLiteral->uniform_normtrans, 1, GL_FALSE, glm::value_ptr(computeNormalMatrix(modelmatrix)));
    glUniformMatrix4fv(shaderLiteral->uniform_mvp, 1, GL_FALSE, glm::value_ptr(mvp));
    glUniform1i(shaderLiteral->uniform_texture, 0);
    glDrawElements(GL_TRIANGLES, e->getModel()->getAmtElements(), GL_UNSIGNED_INT, 0);
    glDisableVertexAttribArray(shaderLiteral->attribute_coord3d);
    glDisableVertexAttribArray(shaderLiteral->attribute_normal);
    glDisableVertexAttribArray(shaderLiteral->attribute_absnormal);
    glDisableVertexAttribArray(shaderLiteral->attribute_texcoord);
  }
  bool Render::free() {
  }
}
