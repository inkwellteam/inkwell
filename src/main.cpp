#include <render.hpp>
#include <session.hpp>
#include <console.hpp>
#include <InkwellConfig.h>

int main(int argc, char* argv[]) {
  inkwell::console->log(&(inkwell::logsource_main), "Inkwell version " INK_VERSION_STR " starting up.");
  inkwell::Session s(argc, argv);
  s.start();
}
