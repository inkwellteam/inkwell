#include <iostream>
#include <sstream>
#include <model.hpp>
#include <shader.hpp>
#include <constants.hpp>
#include <GL/glew.h>
#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

namespace bfs = boost::filesystem;
namespace inkwell {
  Model::Model() {
    bfs::ifstream *fst = new bfs::ifstream(
#ifndef INK_DEBUG_VERSION
      "../"
#endif
      "data/models/pighead.igeom"
    );
    console->err(&logsource_io, loadIGEOM(fst));
    delete fst;
    texture = new sf::Texture();
    if(!texture->loadFromFile(
#ifndef INK_DEBUG_VERSION
      "../"
#endif
      "data/textures/pighead.png"
      )) {
      console->err(&logsource_render, std::string("Failed to load texture"));
    }
    texture->setSmooth(true);
  }
  char lchfs(std::istream *istr) {
    return char(istr->get());
  }
  unsigned short lshfs(std::istream *istr) {
    short i;
    istr->read((char*)(&i), 2);
    return be16toh(i);
  }
  unsigned int linfs(std::istream *istr) {
    int i;
    istr->read((char*)(&i), 4);
    return be32toh(i);
  }
  unsigned long llofs(std::istream *istr) {
    long i;
    istr->read((char*)(&i), 8);
    return be64toh(i);
  }
  float lflfs(std::istream *istr) {
    int i;
    istr->read((char*)(&i), 4);
    i = be32toh(i);
    float *f;
    f = (float*)(&i);
    return *f;
  }
  #define IGEOM_VERSION 1
  std::string Model::loadIGEOM(std::istream *istr, bool *success) {
    if(lshfs(istr) != 0xCEF5) {
      if(success != NULL) *success = false;
      return std::string("It's not an IGEOM file!");
    }
    if(lshfs(istr) != 0x5425) {
      if(success != NULL) *success = false;
      return std::string("It's an Inkwell file, but not an IGEOM file.");
    }
    short gevers = lshfs(istr);
    if(gevers > IGEOM_VERSION) {
      if(success != NULL) *success = false;
      std::stringstream ss;
      ss << "Format version: " << gevers << ", supported: " << IGEOM_VERSION;
      return ss.str();
    }
    // TODO: Add bone stuff
    bool arm_ex = true;
    if(gevers > 0) {
      unsigned char rlength = 0;
      rlength = lchfs(istr);
      arm_ex = (rlength == 0);
      if(!arm_ex) {
        istr->seekg(rlength+1, std::ios_base::cur);
      }
    }
    if(arm_ex) {
      istr->seekg(1, std::ios_base::cur);
      short bl = lshfs(istr);
      istr->seekg(bl, std::ios_base::cur);
    }

    int pointl = linfs(istr);
    glm::vec3 *points = new glm::vec3[pointl];
    for(int i = 0; i < pointl; i++) {
      glm::vec3 *point = points+i;
      point->x = lflfs(istr);
      point->y = lflfs(istr);
      point->z = lflfs(istr);
      if(arm_ex)
        istr->seekg(2, std::ios_base::cur);
    }

    int vertl = linfs(istr);
    MVertex *verts = new MVertex[vertl];
    int *pmap = new int[vertl];
    std::vector<int> *vmap = new std::vector<int>[pointl];
    for(int i = 0; i < vertl; i++) {
      MVertex *vert = verts+i;
      pmap[i] = linfs(istr);
      vert->point = points[pmap[i]];
      vmap[pmap[i]].push_back(i);
      vert->norm.x = lflfs(istr);
      vert->norm.y = lflfs(istr);
      vert->norm.z = lflfs(istr);
      vert->texcoord.x = lflfs(istr);
      vert->texcoord.y = lflfs(istr);
    }

    int tril = linfs(istr)*3;
    GLuint *tris = new GLuint[tril];
    glm::vec3 *absnorms = new glm::vec3[pointl];
    for(int i = 0; i < tril; i+=3) {
      GLuint *tri = tris+i;
      tri[0] = linfs(istr);
      tri[1] = linfs(istr);
      tri[2] = linfs(istr);
      glm::vec3 norm = glm::normalize(glm::cross(verts[tri[1]].point - verts[tri[0]].point, verts[tri[2]].point - verts[tri[0]].point));
      for(unsigned char j = 0; j < 3; j++) {
        int p = pmap[tri[j]];
        absnorms[p] = absnorms[p] + norm;
      }
    }
    for(int i = 0; i < pointl; i++) {
      glm::vec3 absnorm = glm::normalize(absnorms[i]);
      std::vector<int> *v = vmap + i;
      int s = v->size();
      for(int j = 0; j < s; j++) {
        int vert = (*v)[j];
        verts[vert].absnorm = absnorm;
      }
    }

    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

    glBufferData(GL_ARRAY_BUFFER, vertl * sizeof(MVertex), verts, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, tril * sizeof(GLuint), tris, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    amtvertices = vertl;
    amtelements = tril;

    /*float *buf = (float*)(verts);
    for(int i = 0; i < vertl * sizeof(MVertex) / sizeof(float); i++) {
      std::cout << buf[i] << std::endl;
    }*/

    delete[] absnorms;
    delete[] pmap;
    delete[] vmap;
    delete[] points;
    delete[] verts;
    delete[] tris;
    return "";
  }
  Model::~Model() {
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ibo);
    delete texture;
  }
}
