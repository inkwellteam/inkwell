#include <SFML/System.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <session.hpp>
#include <stdlib.h>
#include <iostream>
#include <constants.hpp>
#include <string>
#include <sstream>
#include <math.h>

namespace inkwell {
  Console *console;
  void Session::stop() {
    running = false;
  }
  void Session::tick() {
    time += elapsed;
    milliseconds += int(elapsed * 1000.0f);
    r->tick(time);
  }
  void Session::event() {
    sf::Event e;
    while(window->pollEvent(e)) {
      switch(e.type) {
      case sf::Event::Closed: running = false; break;
      case sf::Event::KeyPressed:
        if(e.key.code == sf::Keyboard::Escape) {
          running = false;
        }
        break;
      case sf::Event::Resized: windowSize(e.size.width, e.size.height); break;
      }
    }
  }
  void Session::openWindow() {
    sf::ContextSettings settings;
    settings.depthBits = 24;
    settings.stencilBits = 8;
    settings.antialiasingLevel = 4;
    settings.majorVersion = 3;
    settings.minorVersion = 0;
//#define INK___FULLSCREEN
#ifdef INK___FULLSCREEN
    sf::VideoMode vm = sf::VideoMode::getDesktopMode();
#else
    sf::VideoMode vm(800, 600);
#endif
    this->window = new sf::Window(vm, "Inkwell",
#ifdef INK___FULLSCREEN
    sf::Style::Fullscreen,
#else
    sf::Style::Default,
#endif
        settings);
    windowSize(vm.width, vm.height);
    window->setKeyRepeatEnabled(false);
  }
  void Session::windowSize(int width, int height) {
    glViewport(0, 0, width, height);
    r->vwidth = width; r->vheight = height;
  }
  bool Session::init(Render *render, int argc, char *argv[]) {
    this->r = render;
    this->tps = INK_DEFAULT_TPS;
    this->overload = INK_DEFAULT_OVERLOAD;
  }
  bool Session::start() {
    openWindow();
    r->init();
    sf::Clock clock;
    int time = 0;
    int lifetime = 0;
    int nextsecond = 1000;
    int tickdelay = 1000 / tps;
    int fps = 0;
    running = true;
    tick();
    while(running) {
      //Handle events
      event();

      //Add to the time and see if we are overloaded
      int atime = clock.getElapsedTime().asMilliseconds();
      time += atime;
      lifetime += atime;
      clock.restart();

      if(time > tickdelay * overload) {
        console->err(&logsource_tick, std::string("Can't keep up! Time skipping or a slow computer?"));
        std::stringstream ss;
        ss << "Ticks skipped: " << (time / tickdelay) - overload;
        console->err(&logsource_tick, ss.str());
        time = 0;
        for(int i = 0; i < overload; i++) {
          elapsed = 1.0f / float(tps);
          tick();
        }
      }

      //Tick
      while(time >= tickdelay) {
        elapsed = 1.0f / float(tps);
        tick();
        time -= tickdelay;
      }
      //Render
      render();
      fps++;
      if(lifetime >= nextsecond) {
        nextsecond += 1000;
        std::stringstream ss;
        ss << "FPS: " << fps;
        console->log(&logsource_tick, ss.str());
        fps = 0;
      }

      sf::sleep(sf::milliseconds(std::max(tickdelay-time, 10)));
      //sf::sleep(sf::milliseconds(std::rand() / (256 * 256 * 128) + 256));
    }
  }
}
