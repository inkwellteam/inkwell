#include <shader.hpp>
#include <console.hpp>
#include <constants.hpp>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <GL/glew.h>

namespace bfs = boost::filesystem;
namespace inkwell {
  Shader::Shader(std::string fname, std::string vname) {
    GLuint vertex;
    GLuint frag;
    if(!compileShader(vname, GL_VERTEX_SHADER, &vertex)) {
      console->err(&logsource_render, "Failed to load vertex shader " + vname);
    } else {
      if(!compileShader(fname, GL_FRAGMENT_SHADER, &frag)) {
        console->err(&logsource_render, "Failed to load fragment shader " + fname);
      } else {
        if(!linkShaders(vertex, frag, &prog)) {
          console->err(&logsource_render, "Failed to link shaders " + fname + "/" + vname);
        } else {
          console->log(&logsource_render, "Loaded shader program " + fname + "/" + vname);
          attribute_coord3d = locateAttribute("coord3d");
          attribute_normal = locateAttribute("normal");
          attribute_absnormal = locateAttribute("absnormal");
          attribute_texcoord = locateAttribute("texcoord");
          uniform_mvp = locateUniform("mvp");
          uniform_normtrans = locateUniform("normtrans");
          uniform_texture = locateUniform("texture");
        }
      }
    }
  }
  // Returns true upon success
  bool Shader::linkShaders(GLuint vertex, GLuint frag, GLuint *program) {
    program[0] = glCreateProgram();
    glAttachShader(program[0], vertex);
    glAttachShader(program[0], frag);
    glLinkProgram(program[0]);
    GLint link_ok = GL_FALSE;
    glGetProgramiv(program[0], GL_LINK_STATUS, &link_ok);
    if(!link_ok) {
      console->err(&logsource_render, "Failed to link shaders:\n" + getLog(program[0]));
      return false;
    }
    return true;
  }
  // Returns true upon success
  bool Shader::compileShader(std::string fname, GLenum type, GLuint *shad) {
    try {
      bfs::path p(
#ifndef INK_DEBUG_VERSION
      "../"
#endif
        "data/shaders/" + fname
        );
      if(bfs::exists(p)) {
        if(bfs::is_regular_file(p)) {
          bfs::ifstream ifs(p);
          std::string cont((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
          const GLchar* source = cont.c_str();
          GLuint res = glCreateShader(type);
          const GLchar* sources[2] = {
#ifdef GL_ES_VERSION_2_0
            "#define GLES2\n"
            "#line 0\n",
#else
            "",
#endif
            source
          };
          glShaderSource(res, 2, sources, NULL);
          glCompileShader(res);
          GLint compile_ok = GL_FALSE;
          glGetShaderiv(res, GL_COMPILE_STATUS, &compile_ok);
          if(compile_ok == GL_FALSE) {
            console->err(&logsource_render, "Failed to compile shader [" + fname + "]:\n" + getLog(res));
          }
          shad[0] = res;
          return true;
        } else if(bfs::is_directory(p)) {
          std::stringstream ss;
          ss << "Failed to load shader [" << fname << "]: File data/shaders/" << fname << " is a directory, not a file";
          console->err(&logsource_io, ss.str());
          return false;
        } else {
          std::stringstream ss;
          ss << "Failed to load shader [" << fname << "]: File data/shaders/" << fname << " is not a file, or a directory";
          console->err(&logsource_io, ss.str());
          return false;
        }
      } else {
        std::stringstream ss;
        ss << "Failed to load shader [" << fname << "]: File data/shaders/" << fname << " does not exist";
        console->err(&logsource_io, ss.str());
        return false;
      }
    } catch(const bfs::filesystem_error ex) {
      console->err(&logsource_io, ex.what());
    }
  }
  GLint Shader::locateAttribute(const char *attribute_name) const {
    GLint attr = glGetAttribLocation(prog, attribute_name);
    if(attr == -1) {
      console->err(&logsource_render, std::string("Could not bind attribute ") + attribute_name);
      return 0;
    }
    return attr;
  }
  GLint Shader::locateUniform(const char *uniform_name) const {
    GLint uni = glGetUniformLocation(prog, uniform_name);
    if(uni == -1) {
      console->err(&logsource_render, std::string("Could not bind uniform ") + uniform_name);
      return 0;
    }
    return uni;
  }
  void Shader::bind() {
    glUseProgram(prog);
  }
  std::string Shader::getLog(GLuint component) {
    GLint log_len;
    if (glIsShader(component))
      glGetShaderiv(component, GL_INFO_LOG_LENGTH, &log_len);
    else if (glIsProgram(component))
      glGetProgramiv(component, GL_INFO_LOG_LENGTH, &log_len);
    else {
      return "printlog: Not a shader or a program\n";
    }
    char* log = (char*)malloc(log_len);

    if (glIsShader(component)) {
      glGetShaderInfoLog(component, log_len, NULL, log);
    } else if (glIsProgram(component)) {
      glGetProgramInfoLog(component, log_len, NULL, log);
    }
    return std::string(log);
  }
  Shader::~Shader() {
    glDeleteProgram(prog);
  }
}
