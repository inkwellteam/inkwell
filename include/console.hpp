#ifndef _INK_CONSOLE_HPP
#define _INK_CONSOLE_HPP

#include <stdlib.h>
#include <string>
//#include <constants.hpp>

namespace inkwell {
  // A class identifying a source for a console message, such as
  // the render engine or a resource loader
  class LogSource {
  private:
    std::string name, spaces;
  public:
    LogSource(std::string name);
    ~LogSource() {};
    std::string getName() const { return name; }
    std::string getSpaces() const { return spaces; }
  };
  // A class that manages output to the in-game console, the
  // command line, and anything else logging-related
  class Console {
  public:
  private:
    std::string setColor(int n);
    std::string resetStyle();
    std::string setBold();
    std::string setUnBold();
  public:
    Console() {};
    ~Console() {};
    // Logging and error-messaging
    void log(const LogSource *ls, std::string msg);
    void err(const LogSource *ls, std::string msg);
  };

  // Constant log sources; pretty self-explanatory.
  const LogSource logsource_render("Render");
  const LogSource logsource_tick("Time");
  const LogSource logsource_main("Main");
  const LogSource logsource_io("I/O");

  extern Console *console;
  /*
    I use the extern keyword here because the extern keyword keeps the linker from assigning memory to the variable.
    This is important, because although the header guard provides some protection, multiple object files will include
    this header file, defining console for each of those object files. The linker takes these object files, and notices
    this conflict.
   */
}

#endif
