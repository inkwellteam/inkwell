#ifndef _INK_SESSION_HPP
#define _INK_SESSION_HPP

#include <SFML/Window.hpp>
#include <constants.hpp>
#include <render.hpp>
#include <console.hpp>

namespace inkwell {
  class Session {
  private:
    // Ticks per second
    unsigned short tps;
    // How many ticks we can be overloaded by
    unsigned short overload;
    // Are we running?
    bool running;
    // Render engine
    Render *r;
    // Our portal in to the artificial universe
    sf::Window *window;
    // Time elapsed, in seconds and milliseconds.
    double time;
    long milliseconds;
    // The amount of time since the last tick
    float elapsed;

  public:
    Session(Render *r, int argc, char *argv[]) {
      console = new Console();
      init(r, argc, argv);
    }
    Session(int argc, char *argv[]) {
      console = new Console(); 
      Render *render = new Render();
      init(render, argc, argv);
    }
    void windowSize(int width, int height);
    bool init(Render *render, int argc, char *argv[]);
    bool start();
    void event();
    void tick();
    void render() { r->render(); window->display(); }
    void stop();
    void openWindow();
    Console *getConsole() { return console; }
    Render *getRender() { return r; }
    ~Session() { delete r; r = NULL; delete console; console = NULL; delete window; window = NULL; };
  };
}

#endif
