#ifndef _INK_MODEL_HPP
#define _INK_MODEL_HPP
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <SFML/Graphics.hpp>
#include <GL/glew.h>
namespace inkwell {
  class Model {
  private:
    GLuint vbo;
    GLuint ibo;
    int amtvertices;
    int amtelements;
  public:
    Model();
    GLuint getVBO() const { return vbo; }
    GLuint getIBO() const { return ibo; }
    int getAmtVertices() const { return amtvertices; }
    int getAmtElements() const { return amtelements; }
    // Returns an error message, or an empty string if there is none.
    std::string loadIGEOM(std::istream *istr, bool *success = NULL);
    sf::Texture *texture;
    ~Model();
  };
  typedef struct MVertex {
    glm::vec3 point;
    glm::vec3 norm;
    glm::vec3 absnorm;
    glm::vec2 texcoord;
  } MVertex;
}
#endif
