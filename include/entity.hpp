#ifndef _INK_ENTITY_HPP
#define _INK_ENTITY_HPP

#include <model.hpp>
namespace inkwell {
  class Entity {
  private:
    Model *model;
  public:
    double x,y,z;
    float rx,ry,rz;
    Entity(Model *model);
    Model *getModel() const;
  };
}
#endif
