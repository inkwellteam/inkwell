#ifndef _INK_RENDER_HPP
#define _INK_RENDER_HPP

#include <SFML/Window.hpp>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <shader.hpp>
#include <entity.hpp>

namespace inkwell {
  // An Inkwell renderer. Uses OpenGL to render the game.
  class Render {
  private:
    sf::Window *window;
    Shader *shaderLiteral;
    Entity *triangle;
    glm::mat4 vp;
  public:
    GLint vwidth, vheight;
    Render() { };
    ~Render() {free();};
    
    // Inline functions
    glm::mat3 computeNormalMatrix(glm::mat4 mvp) const {
      return glm::transpose(glm::inverse(glm::mat3(mvp)));
    }
    
    // Uh... Non-inline functions? What are these called?
    // Initializes the renderer; returns true on success.
    bool init();
    void computeViewProjection();
    glm::mat4 computeMatrix(Entity *e) const;
    bool loadShaders();
    void handleEvents();
    void render();
    void renderEntity(Entity *e);
    bool free();
    void tick(double time);
  };
}

#endif
