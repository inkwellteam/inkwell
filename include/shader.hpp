#ifndef _INK_SHADER_HPP
#define _INK_SHADER_HPP
#include <string>
#include <GL/glew.h>
namespace inkwell {
  class Shader {
  private:
    GLuint prog;
    std::string getLog(GLuint component);
  public:
    /*
      Loads the shader program with the fragment shader
      stored in file fname and the vertex shader in
      file vname
    */
    Shader(std::string fname, std::string vname);
    bool compileShader(std::string, GLenum, GLuint *shad);
    bool linkShaders(GLuint vertex, GLuint frag, GLuint *prog);
    GLint locateAttribute(const char *attribute_name) const;
    GLint locateAttribute(const std::string attribute_name) const {
      return locateAttribute(attribute_name.c_str());
    }
    GLint locateUniform(const char *uniform_name) const;
    GLint locateUniform(const std::string uniform_name) const{
      return locateUniform(uniform_name.c_str());
    }
    void bind();
    GLint attribute_coord3d;
    GLint attribute_normal;
    GLint attribute_absnormal;
    GLint attribute_texcoord;
    GLint uniform_mvp;
    GLint uniform_normtrans;
    GLint uniform_texture;
    ~Shader();
  };
}
#endif
