#ifndef _INK_CONSTANTS_HPP
#define _INK_CONSTANTS_HPP
#include <console.hpp>

// The default amount of ticks per second in a game
#define INK_DEFAULT_TPS 60
// The default amountof ticks allowed to be queued before resetting
#define INK_DEFAULT_OVERLOAD 40
// The maximum length for a console log source's name
#define INK_CONS_LS_LENGTH 10

// Uncomment if this is a debug version
#define INK_DEBUG_VERSION

#endif
