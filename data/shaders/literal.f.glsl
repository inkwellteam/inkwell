#version 120
varying vec3 f_normal;
varying vec2 f_texcoord;
varying float f_diffuse;
uniform sampler2D texture;
void main(void) {
  float specular;
  if(dot(normalize(f_normal), normalize(vec3(1.0, 1.0, -0.5))) < 0.0) {
    specular = 0.0;
  } else {
    specular = pow(max(0.0, dot(reflect(-normalize(vec3(1.0, 1.0, -0.5)), normalize(f_normal)), vec3(0.0, 0.0, 1.0))), 10.0);
  }
  gl_FragColor = texture2D(texture, vec2(f_texcoord.x, 1.0-f_texcoord.y)) * (f_diffuse + specular * 0.5);
}
