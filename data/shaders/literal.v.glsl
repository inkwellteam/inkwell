#version 120
attribute vec3 coord3d;
attribute vec3 normal;
attribute vec3 absnormal;
attribute vec2 texcoord;
varying vec3 f_normal;
varying vec2 f_texcoord;
varying float f_diffuse;
uniform mat4 mvp;
uniform mat3 normtrans;
void main(void) {
  gl_Position = mvp * vec4(coord3d + absnormal * 0.0000001, 1.0);
  f_normal = normtrans * normal;
  f_diffuse = clamp(dot(normalize(f_normal),normalize(vec3(1.0, 1.0, -0.5))) * 0.5 + 0.75, 0.0, 1.0);
  f_texcoord = texcoord;
}
